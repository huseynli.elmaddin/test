import java.lang.reflect.Array;
import java.util.HashMap;

public class Students {
    Integer id;
    String name;
    String surname;
    int age;
    String coursName;

    public Students(Integer id,String name, String surname, int age, String coursName){
        this.id=id;
        this.name=name;
        this.surname=surname;
        this.age=age;
        this.coursName=coursName;
    };

    public Students() {

    }

    @Override
    public String toString() {
        return "Student id : "+id+" Students information  : " +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", coursName='" + coursName + '\''
                ;
    }
}
